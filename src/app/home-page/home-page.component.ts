import { Component, OnInit } from '@angular/core';
import { RoutingService } from '../routing.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  constructor(private wow:RoutingService) { }

  ngOnInit() {
  }

  navigate(page:Number){
    this.wow.navigate(page);
  }

}
