import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoutingService {

  currentPage : BehaviorSubject<Number> = new BehaviorSubject(0);
  constructor(private router:Router) {}

  navigate(page:Number){
    this.currentPage.next(page);
    switch(page){
      case 0:
        this.router.navigateByUrl("home");
        break;
      case 1:
        this.router.navigateByUrl("service");
        break;
      case 2:
        this.router.navigateByUrl("potfolio");
        break;
      case 3:
        this.router.navigateByUrl("about");
        break;
      case 4:
        this.router.navigateByUrl("contact");
        break;
    }
  }
}
