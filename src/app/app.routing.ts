import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { PotfolioPageComponent } from './potfolio-page/potfolio-page.component';
import { ServicePageComponent } from './service-page/service-page.component';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { AboutPageComponent } from './about-page/about-page.component';

export const appRoutes: Routes = [
    { path: '', redirectTo: "/home", pathMatch: "full" },
    { path: 'home', component: HomePageComponent },
    { path: 'about', component: AboutPageComponent },
    { path: 'contact', component: ContactPageComponent },
    { path: 'service', component: ServicePageComponent },
    { path: 'potfolio', component: PotfolioPageComponent}
];