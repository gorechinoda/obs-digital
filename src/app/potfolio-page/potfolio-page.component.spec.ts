import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PotfolioPageComponent } from './potfolio-page.component';

describe('PotfolioPageComponent', () => {
  let component: PotfolioPageComponent;
  let fixture: ComponentFixture<PotfolioPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PotfolioPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PotfolioPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
