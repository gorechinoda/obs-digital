import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { RoutingService } from './routing.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'obs';
  currentPage:Number = 0;
  constructor(private wow:RoutingService) { 
    this.wow.currentPage.subscribe(data=>{
      this.currentPage = data;
    })
  }

  navigate(page:Number){
    this.wow.navigate(page);
  }
}
