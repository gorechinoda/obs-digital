import { Component, OnInit } from '@angular/core';
import { RoutingService } from '../routing.service';

@Component({
  selector: 'app-service-page',
  templateUrl: './service-page.component.html',
  styleUrls: ['./service-page.component.css']
})
export class ServicePageComponent implements OnInit {

  constructor(private wow:RoutingService) { }

  ngOnInit() {
  }

  navigate(page:Number){
    this.wow.navigate(page);
  }

}
